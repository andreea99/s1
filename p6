def func_6(types):
    if type(types) == int:
        return (lambda x, y: -1 if x < y
                else 1 if x > y
                else 0)
    if type(types) == str:
        return (lambda x, y: -1 if len(x) < len(y)
                else 1 if len(x) > len(y)
                else 0)
    else:
        return (lambda x, y: print('Unknown type!'))


cmp1 = func_6(0.0)
print(cmp1(2.5, 3.2))
cmp2 = func_6('')
print(cmp2('ana are', 'adi are'))
