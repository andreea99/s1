def func_4(nume_1="countries.txt", nume_2="visited.txt", nume_3="to_be_visited.txt"):
    f = open(nume_1, 'r')
    lc = f.readlines()
    g = open(nume_2, 'r')
    lv = g.readlines()
    h = open(nume_3, 'w')
    for i in lc:
        if i not in lv:
            h.write(i)
    f.close()
    g.close()
    h.close()


func_4()
